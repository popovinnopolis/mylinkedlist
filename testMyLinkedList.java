package MyLinkedList;

/**
 * Created by evgenijpopov on 07.06.17.
 */
public class testMyLinkedList {
    public static void main(String[] args) {
        MyLinkedList<Integer> a = new MyLinkedList<Integer>();
        System.out.println("Initial state: " + a);
        a.Add(1);
        a.Add(3);
        a.Add(2);
        a.Add(3);
        System.out.println("Adding some elements: " + a);

        System.out.println("Is contains 3: " + a.Contains(3));

        a.AddAt(42, 2);
        System.out.println("After add 42 at index 2: " + a);

        a.Remove(3);

        System.out.println("After remove 3: " + a);
        System.out.println("Is contains 3: " + a.Contains(3));

        a.RemoveAt(0);
        System.out.println("After remove node at 0: " + a);
    }
}
