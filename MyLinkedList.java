package MyLinkedList;

/**
 * Created by evgenijpopov on 07.06.17.
 */
public class MyLinkedList<T> {
    private int size = 0;
    private Node<T> head;
    private Node<T> tail;

    public MyLinkedList() {
    }

    public void Add(T item) {
        RelinkLast(item);
    }

    public void AddAt(T item, int index) {
        if (size < index) throw new IndexOutOfBoundsException();
        Node<T> currNode = head;
        int counter = 0;
        while (currNode != null) {
            if (index == counter) {
                RelinkInsteadNode(currNode, item);
                break;
            }
            currNode = currNode.next;
            counter++;
        }
    }

    public boolean Remove(T item) {
        if (size == 0) return false;
        Node<T> currNode = head;
        boolean res = false;
        while (currNode != null) {
            if (currNode.item.equals(item)) {
                UnlinkNode(currNode);
                res = true;
            }
            currNode = currNode.next;
        }
        return res;
    }

    public boolean RemoveAt(int index) {
        if (size == 0) return false;
        Node<T> currNode = head;
        boolean res = false;
        int counter = 0;
        while (currNode != null) {
            if (index == counter) {
                UnlinkNode(currNode);
                res = true;
                break;
            }
            currNode = currNode.next;
            counter++;
        }
        return res;
    }

    public boolean Contains(T item) {
        if (size == 0) return false;
        Node<T> currNode = head;
        boolean res = false;
        while (currNode != null) {
            if (currNode.item.equals(item)) {
                res = true;
                break;
            }
            currNode = currNode.next;
        }
        return res;
    }

    private void RelinkInsteadNode(Node<T> node, T item) {
        if (node == null) throw new NullPointerException();
        Node<T> prevNode = node.prev;

        Node<T> newNode = new Node<T>(prevNode, node, item);
        node.prev = newNode;
        if (prevNode == null) {
            head = newNode;
        } else {
            prevNode.next = newNode;
        }
        size++;
    }

    private void UnlinkNode(Node<T> node) {
        Node<T> prevNode = node.prev;
        Node<T> nextNode = node.next;
        if (prevNode == null) {
            SetHeadNode(nextNode);
        } else {
            prevNode.next = nextNode;
        }

        if (nextNode == null) {
            SetTailNode(prevNode);
        } else {
            nextNode.prev = prevNode;
        }
        size--;
    }

    private void RelinkLast(T item) {
        if (size == 0) {
            head = tail = new Node<T>(null, null, item);
        } else {
            Node<T> oldTail = tail;
            Node<T> node = new Node<T>(tail, null, item);
            tail = node;
            oldTail.next = tail;
        }
        size++;
    }

    private void SetHeadNode(Node<T> node){
        head = node;
        node.prev = null;
    }

    private void SetTailNode(Node<T> node){
        tail = node;
        node.next = null;
    }

    public String toString(){
        if (size == 0) return "";
        Node<T> currNode = head;
        StringBuilder res = new StringBuilder();
        res.append("[");
        while (currNode != null) {
            res.append(currNode.item);
            if (currNode.next != null) res.append(", ");
            currNode = currNode.next;
        }
        res.append("]");
        return res.toString();
    }

    private static class Node<T> {
        T item;
        Node<T> prev;
        Node<T> next;

        Node(Node<T> prev, Node<T> next, T item) {
            this.item = item;
            this.prev = prev;
            this.next = next;
        }
    }

}
